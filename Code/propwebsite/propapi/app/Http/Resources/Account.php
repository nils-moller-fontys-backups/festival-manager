<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Account extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /*
         *  return [
         *      'email' => $this->Email
         *      'firstName' => $this->FirstName
         *      .....
         *      'local name' => $this->DBattribute
         */
        return parent::toArray($request);
    }
    public function with($request) //this gets send with every request WITHOUT specifying anything
    {
        return [
            'API version' => '1.0.0',
            'author' => 'Nils Möller'
        ];
    }
}
