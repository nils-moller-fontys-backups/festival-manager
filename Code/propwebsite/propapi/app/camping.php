<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class camping extends Model
{
    public $timestamps = false;
    protected $table = 'campings';
    protected $fillable = [
        'isAvailable'
    ];
}
