<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//ACCOUNTS

//list all accounts
Route::get('accounts', 'AccountsController@index');

//create new account
Route::post('newAccount', 'AccountsController@store');

//update phone
Route::put('updatePhone/{email}', 'AccountsController@updatePhone');

//update password
Route::put('updatePassword/{email}', 'AccountsController@updatePassword');

//delete account
Route::delete('account/{email}', 'AccountsController@destroy');

//get account by email
Route::get('account/{email}', 'AccountsController@showByEmail');

//list account by email+password
Route::get('account/{email}/{password}', 'AccountsController@showByEmailPassword');

//add credit
Route::put('addCredit/{email}', 'AccountsController@addCredit');


//TICKETS

//list all tickets
Route::get('tickets', 'TicketsController@index');

//list single ticket
Route::get('ticket/{id}', 'TicketsController@show');

//create new ticket
Route::post('newticket', 'TicketsController@store');

//delete ticket
Route::delete('deleteticket/{Id}', 'TicketsController@destroy');



//CAMPINGS

//list all campings
Route::get('campings', 'CampingController@index');

//list single camping
Route::get('camping/{id}', 'CampingController@show');

//create new camping
Route::post('newcamping', 'CampingController@store');

//delete camping
Route::delete('deletecamping/{Id}', 'CampingController@destroy');

//update camping
Route::put('updateCamping/{id}', 'CampingController@update');

//get first free camping
Route::get('freespot', 'CampingController@freespot');
