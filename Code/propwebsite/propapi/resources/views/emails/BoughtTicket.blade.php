<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
</head>
<body>
<h1>Ticket paid for and your welcome package with RFID entry key is underway!</h1>
<p>{{$title}}</p>
</body>
</html>