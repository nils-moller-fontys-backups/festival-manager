import { Component, OnInit } from '@angular/core';
import {Ticket} from '../classes/ticket';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  sessionticket: Ticket = JSON.parse(sessionStorage.getItem('ticket'));

  constructor() { }

  ngOnInit() {
  }

}
