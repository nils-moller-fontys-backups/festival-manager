import { TestBed, inject } from '@angular/core/testing';

import { BuyticketService } from './buyticket.service';

describe('BuyticketService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BuyticketService]
    });
  });

  it('should be created', inject([BuyticketService], (service: BuyticketService) => {
    expect(service).toBeTruthy();
  }));
});
