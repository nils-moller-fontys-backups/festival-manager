import { TestBed, inject } from '@angular/core/testing';

import { ApiInteractionService } from './api-interaction.service';

describe('ApiInteractionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiInteractionService]
    });
  });

  it('should be created', inject([ApiInteractionService], (service: ApiInteractionService) => {
    expect(service).toBeTruthy();
  }));
});
