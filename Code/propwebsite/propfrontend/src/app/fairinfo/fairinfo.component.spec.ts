import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FairinfoComponent } from './fairinfo.component';

describe('FairinfoComponent', () => {
  let component: FairinfoComponent;
  let fixture: ComponentFixture<FairinfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FairinfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FairinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
