import {Component} from '@angular/core';
import {OnInit} from '@angular/core';
import {LoginService} from './login.service';
import {Account} from './classes/account';
import {Ticket} from './classes/ticket';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

    title = 'Rockfair';
    sessionAccount: Account = JSON.parse(sessionStorage.getItem('account'));
    sessionTicket: Ticket = JSON.parse(sessionStorage.getItem('ticket'));

    constructor(public LoginService: LoginService){
    }

    ngOnInit(){
    }

    logOut(){
        this.LoginService.logOut();
    }
}
