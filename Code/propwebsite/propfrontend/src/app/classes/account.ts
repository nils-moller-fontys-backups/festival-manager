export class Account {
    Email: string;
    Name: string;
    Password: string;
    Gender: number;
    Phone: string;
    CheckedInEvent: boolean;
    CreditStored: number;
    CheckedInCamping: boolean;
    Address: string;
    DateOfBirth: string;
    RFID: string;
    TicketId: number;
    PaidForTicket: boolean;
    
    /*
    database account storage:
    Email - primary - string
    Name - string
    Password - string
    Gender - varchar(6)
    Phone - string
    CheckedInEvent - 0/1
    CreditStored - number
    CheckedInCamping - 0/1
    Address - string
    dateofbirth - varchar(50)
    RFID - varchar
    TicketId - int(11)
    PaidForTicket - tinyint(4)
     */
    
    constructor(Email: string, Name: string, Password: string, Gender: number, Phone: string, Address: string, DateOfBirth: string) {
        this.Email = Email;
        this.Name = Name;
        this.Password = Password;
        this.Gender = Gender;
        this.Phone = Phone;
        this.CheckedInEvent = false;
        this.CheckedInCamping = false;
        this.CreditStored = 0;
        this.Address = Address;
        this.DateOfBirth = DateOfBirth;
        this.RFID = null;
        this.TicketId = null;
        this.PaidForTicket = false;
    }
}
