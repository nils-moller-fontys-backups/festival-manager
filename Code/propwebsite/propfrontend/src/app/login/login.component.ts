import {Component, OnInit} from '@angular/core';
import {LoginService} from '../login.service';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    email: string;
    password: string;
    subscription: Subscription;

    constructor(private LoginService: LoginService) { }

    ngOnInit() {
    }

    logIn(){
        this.subscription = this.LoginService.logIn(this.email, this.password).subscribe();
    }
    
    ngDestroy(){
        if (this.subscription)
            this.subscription.unsubscribe();
    }
}
