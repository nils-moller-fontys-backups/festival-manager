﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Admin_App;
using ATMLog;
using BuyItem;
using Camping_Gate;
using Entrance_Gate;
using InfoDesk;
using RentItem;

namespace collectiveForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnOpenAdmin_Click(object sender, EventArgs e)
        {
            Admin_App.Form1 f1 = new Admin_App.Form1();
            f1.Show();
            
        }

        private void btnOpenATM_Click(object sender, EventArgs e)
        {
            ATMLog.bt10 f1 = new ATMLog.bt10();
            f1.Show();
            
        }

        private void btnOpenBuyItem_Click(object sender, EventArgs e)
        {
            BuyItem.Form1 f1 = new BuyItem.Form1();
            f1.Show();
            
        }

        private void btnOpenCamping_Click(object sender, EventArgs e)
        {
            Camping_Gate.Form1 f1 = new Camping_Gate.Form1();
            f1.Show();
            
        }

        private void btnOpenEntrance_Click(object sender, EventArgs e)
        {
            Entrance_Gate.Form1 f1 = new Entrance_Gate.Form1();
            f1.Show();
            
        }

        private void btnOpenInfoDesk_Click(object sender, EventArgs e)
        {
            InfoDesk.Form1 f1 = new InfoDesk.Form1();
            f1.Show();
            
        }

        private void btnOpenRentItem_Click(object sender, EventArgs e)
        {
            RentItem.Form1 f1 = new RentItem.Form1();
            f1.Show();
            
        }
    }
}
