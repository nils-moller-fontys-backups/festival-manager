﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuyItem
{
    class Customer
    {

        public string Email { get; set; }
        public string AccountId { get; set; }
        public double Credit { get; set; }

        public Customer(string email, string accountid, double credit)
        {
            this.Email = email;
            this.AccountId = accountid;
            this.Credit = credit;
        }

        public string GetAccountId()
        {
            return this.AccountId;
        }

    }
}
