﻿namespace Admin_App
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnInfoUpdate = new System.Windows.Forms.Button();
            this.lblTotalDeposit = new System.Windows.Forms.Label();
            this.lblPeopleAtFair = new System.Windows.Forms.Label();
            this.lblNrTicketsSold = new System.Windows.Forms.Label();
            this.lblUnreturnedItems = new System.Windows.Forms.Label();
            this.lblConsumablesSold = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbAtmTransactionLog = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chtCheckIn = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chtDeposits = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.btnProcess = new System.Windows.Forms.Button();
            this.lblFileName = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnFillListBox = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnRefresh = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chtCheckIn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chtDeposits)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(36, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(204, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Net Income from Consumables:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(36, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(250, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Number of items that are not returned:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(36, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(169, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Number of Ticket Bought:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(36, 180);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(221, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Current Number of People at Fair:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(36, 220);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(164, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Total Amount Deposited:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.PaleGreen;
            this.panel1.Controls.Add(this.btnInfoUpdate);
            this.panel1.Controls.Add(this.lblTotalDeposit);
            this.panel1.Controls.Add(this.lblPeopleAtFair);
            this.panel1.Controls.Add(this.lblNrTicketsSold);
            this.panel1.Controls.Add(this.lblUnreturnedItems);
            this.panel1.Controls.Add(this.lblConsumablesSold);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(27, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(480, 281);
            this.panel1.TabIndex = 5;
            // 
            // btnInfoUpdate
            // 
            this.btnInfoUpdate.Location = new System.Drawing.Point(39, 247);
            this.btnInfoUpdate.Name = "btnInfoUpdate";
            this.btnInfoUpdate.Size = new System.Drawing.Size(101, 23);
            this.btnInfoUpdate.TabIndex = 22;
            this.btnInfoUpdate.Text = "Update Info";
            this.btnInfoUpdate.UseVisualStyleBackColor = true;
            this.btnInfoUpdate.Click += new System.EventHandler(this.btnInfoUpdate_Click);
            // 
            // lblTotalDeposit
            // 
            this.lblTotalDeposit.AutoSize = true;
            this.lblTotalDeposit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalDeposit.Location = new System.Drawing.Point(307, 220);
            this.lblTotalDeposit.Name = "lblTotalDeposit";
            this.lblTotalDeposit.Size = new System.Drawing.Size(32, 17);
            this.lblTotalDeposit.TabIndex = 11;
            this.lblTotalDeposit.Text = "€ ...";
            // 
            // lblPeopleAtFair
            // 
            this.lblPeopleAtFair.AutoSize = true;
            this.lblPeopleAtFair.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeopleAtFair.Location = new System.Drawing.Point(307, 180);
            this.lblPeopleAtFair.Name = "lblPeopleAtFair";
            this.lblPeopleAtFair.Size = new System.Drawing.Size(20, 17);
            this.lblPeopleAtFair.TabIndex = 10;
            this.lblPeopleAtFair.Text = "...";
            // 
            // lblNrTicketsSold
            // 
            this.lblNrTicketsSold.AutoSize = true;
            this.lblNrTicketsSold.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNrTicketsSold.Location = new System.Drawing.Point(307, 138);
            this.lblNrTicketsSold.Name = "lblNrTicketsSold";
            this.lblNrTicketsSold.Size = new System.Drawing.Size(20, 17);
            this.lblNrTicketsSold.TabIndex = 9;
            this.lblNrTicketsSold.Text = "...";
            // 
            // lblUnreturnedItems
            // 
            this.lblUnreturnedItems.AutoSize = true;
            this.lblUnreturnedItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnreturnedItems.Location = new System.Drawing.Point(307, 97);
            this.lblUnreturnedItems.Name = "lblUnreturnedItems";
            this.lblUnreturnedItems.Size = new System.Drawing.Size(20, 17);
            this.lblUnreturnedItems.TabIndex = 8;
            this.lblUnreturnedItems.Text = "...";
            // 
            // lblConsumablesSold
            // 
            this.lblConsumablesSold.AutoSize = true;
            this.lblConsumablesSold.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConsumablesSold.Location = new System.Drawing.Point(307, 58);
            this.lblConsumablesSold.Name = "lblConsumablesSold";
            this.lblConsumablesSold.Size = new System.Drawing.Size(32, 17);
            this.lblConsumablesSold.TabIndex = 7;
            this.lblConsumablesSold.Text = "€ ...";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(34, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(366, 25);
            this.label6.TabIndex = 5;
            this.label6.Text = "Administration Referance Information";
            // 
            // lbAtmTransactionLog
            // 
            this.lbAtmTransactionLog.FormattingEnabled = true;
            this.lbAtmTransactionLog.Location = new System.Drawing.Point(27, 351);
            this.lbAtmTransactionLog.Name = "lbAtmTransactionLog";
            this.lbAtmTransactionLog.Size = new System.Drawing.Size(294, 225);
            this.lbAtmTransactionLog.TabIndex = 14;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Coral;
            this.groupBox2.Controls.Add(this.chtCheckIn);
            this.groupBox2.Controls.Add(this.chtDeposits);
            this.groupBox2.Location = new System.Drawing.Point(539, 25);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(664, 558);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Income and Gate Arrivales Charts";
            // 
            // chtCheckIn
            // 
            chartArea1.Name = "ChartArea1";
            this.chtCheckIn.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chtCheckIn.Legends.Add(legend1);
            this.chtCheckIn.Location = new System.Drawing.Point(22, 287);
            this.chtCheckIn.Name = "chtCheckIn";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Entrance Gate Sign In";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Entrance Gate Sign Out";
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Camping Gate Sign In";
            series4.ChartArea = "ChartArea1";
            series4.Legend = "Legend1";
            series4.Name = "Camping Gate Sign Out";
            this.chtCheckIn.Series.Add(series1);
            this.chtCheckIn.Series.Add(series2);
            this.chtCheckIn.Series.Add(series3);
            this.chtCheckIn.Series.Add(series4);
            this.chtCheckIn.Size = new System.Drawing.Size(620, 258);
            this.chtCheckIn.TabIndex = 1;
            this.chtCheckIn.Text = "Gate Tracking";
            // 
            // chtDeposits
            // 
            chartArea2.Name = "ChartArea1";
            this.chtDeposits.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chtDeposits.Legends.Add(legend2);
            this.chtDeposits.Location = new System.Drawing.Point(22, 30);
            this.chtDeposits.Name = "chtDeposits";
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series5.Legend = "Legend1";
            series5.Name = "Deposits";
            this.chtDeposits.Series.Add(series5);
            this.chtDeposits.Size = new System.Drawing.Size(620, 251);
            this.chtDeposits.TabIndex = 0;
            this.chtDeposits.Text = "Transition Tracking";
            this.chtDeposits.Click += new System.EventHandler(this.chart1_Click);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(355, 384);
            this.btnBrowse.Margin = new System.Windows.Forms.Padding(2);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(101, 43);
            this.btnBrowse.TabIndex = 17;
            this.btnBrowse.Text = "Browse a log file";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.Location = new System.Drawing.Point(355, 495);
            this.btnProcess.Margin = new System.Windows.Forms.Padding(2);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(101, 39);
            this.btnProcess.TabIndex = 18;
            this.btnProcess.Text = "Process Logs";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // lblFileName
            // 
            this.lblFileName.AutoSize = true;
            this.lblFileName.Location = new System.Drawing.Point(25, 310);
            this.lblFileName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(51, 13);
            this.lblFileName.TabIndex = 19;
            this.lblFileName.Text = "FileName";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(25, 334);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(37, 13);
            this.lblStatus.TabIndex = 20;
            this.lblStatus.Text = "Status";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(406, 310);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(101, 23);
            this.btnUpdate.TabIndex = 21;
            this.btnUpdate.Text = "Update Chart";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnFillListBox
            // 
            this.btnFillListBox.Location = new System.Drawing.Point(355, 441);
            this.btnFillListBox.Margin = new System.Windows.Forms.Padding(2);
            this.btnFillListBox.Name = "btnFillListBox";
            this.btnFillListBox.Size = new System.Drawing.Size(101, 40);
            this.btnFillListBox.TabIndex = 22;
            this.btnFillListBox.Text = "Show Logs";
            this.btnFillListBox.UseVisualStyleBackColor = true;
            this.btnFillListBox.Click += new System.EventHandler(this.btnFillListBox_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Enabled = false;
            this.btnRefresh.Location = new System.Drawing.Point(406, 339);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(101, 23);
            this.btnRefresh.TabIndex = 23;
            this.btnRefresh.Text = "Refresh Chart";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Visible = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1218, 595);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnFillListBox);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblFileName);
            this.Controls.Add(this.btnProcess);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.lbAtmTransactionLog);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Admin Panel";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chtCheckIn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chtDeposits)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblTotalDeposit;
        private System.Windows.Forms.Label lblPeopleAtFair;
        private System.Windows.Forms.Label lblNrTicketsSold;
        private System.Windows.Forms.Label lblUnreturnedItems;
        private System.Windows.Forms.Label lblConsumablesSold;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListBox lbAtmTransactionLog;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chtCheckIn;
        private System.Windows.Forms.DataVisualization.Charting.Chart chtDeposits;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.Label lblFileName;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnFillListBox;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnInfoUpdate;
    }
}

