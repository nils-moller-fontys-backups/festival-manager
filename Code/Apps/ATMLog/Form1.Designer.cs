﻿namespace ATMLog
{
    partial class bt10
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnConnect = new System.Windows.Forms.Button();
            this.lblRFIDstatus = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbaccountidcode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gbinput = new System.Windows.Forms.Panel();
            this.buttonNone = new System.Windows.Forms.Button();
            this.buttonNone2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.bt3 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.bt1 = new System.Windows.Forms.Button();
            this.bt6 = new System.Windows.Forms.Button();
            this.bt5 = new System.Windows.Forms.Button();
            this.bt4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.bt8 = new System.Windows.Forms.Button();
            this.bt7 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.tbDeposit = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDeposit5 = new System.Windows.Forms.Button();
            this.btnDeposit10 = new System.Windows.Forms.Button();
            this.btnDeposit50 = new System.Windows.Forms.Button();
            this.btnDeposit20 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.gbinput.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.btnConnect);
            this.panel1.Controls.Add(this.lblRFIDstatus);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.tbaccountidcode);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(16, 121);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(299, 301);
            this.panel1.TabIndex = 5;
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(92, 171);
            this.btnConnect.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(73, 33);
            this.btnConnect.TabIndex = 3;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // lblRFIDstatus
            // 
            this.lblRFIDstatus.AutoSize = true;
            this.lblRFIDstatus.Location = new System.Drawing.Point(69, 225);
            this.lblRFIDstatus.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRFIDstatus.Name = "lblRFIDstatus";
            this.lblRFIDstatus.Size = new System.Drawing.Size(37, 13);
            this.lblRFIDstatus.TabIndex = 2;
            this.lblRFIDstatus.Text = "Status";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("SimSun", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(87, 42);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(125, 29);
            this.label4.TabIndex = 0;
            this.label4.Text = "Account";
            // 
            // tbaccountidcode
            // 
            this.tbaccountidcode.Location = new System.Drawing.Point(92, 127);
            this.tbaccountidcode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tbaccountidcode.Name = "tbaccountidcode";
            this.tbaccountidcode.Size = new System.Drawing.Size(168, 20);
            this.tbaccountidcode.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 131);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Email:";
            // 
            // gbinput
            // 
            this.gbinput.BackColor = System.Drawing.Color.CornflowerBlue;
            this.gbinput.Controls.Add(this.buttonNone);
            this.gbinput.Controls.Add(this.buttonNone2);
            this.gbinput.Controls.Add(this.button1);
            this.gbinput.Controls.Add(this.btnClear);
            this.gbinput.Controls.Add(this.bt3);
            this.gbinput.Controls.Add(this.button8);
            this.gbinput.Controls.Add(this.bt1);
            this.gbinput.Controls.Add(this.bt6);
            this.gbinput.Controls.Add(this.bt5);
            this.gbinput.Controls.Add(this.bt4);
            this.gbinput.Controls.Add(this.button3);
            this.gbinput.Controls.Add(this.bt8);
            this.gbinput.Controls.Add(this.bt7);
            this.gbinput.Controls.Add(this.label5);
            this.gbinput.Location = new System.Drawing.Point(332, 222);
            this.gbinput.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbinput.Name = "gbinput";
            this.gbinput.Size = new System.Drawing.Size(312, 326);
            this.gbinput.TabIndex = 6;
            // 
            // buttonNone
            // 
            this.buttonNone.Location = new System.Drawing.Point(170, 250);
            this.buttonNone.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonNone.Name = "buttonNone";
            this.buttonNone.Size = new System.Drawing.Size(46, 38);
            this.buttonNone.TabIndex = 5;
            this.buttonNone.UseVisualStyleBackColor = true;
            // 
            // buttonNone2
            // 
            this.buttonNone2.Location = new System.Drawing.Point(34, 250);
            this.buttonNone2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonNone2.Name = "buttonNone2";
            this.buttonNone2.Size = new System.Drawing.Size(46, 38);
            this.buttonNone2.TabIndex = 4;
            this.buttonNone2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(101, 250);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(46, 38);
            this.button1.TabIndex = 3;
            this.button1.Text = "0";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(233, 139);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(57, 74);
            this.btnClear.TabIndex = 2;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // bt3
            // 
            this.bt3.Location = new System.Drawing.Point(170, 188);
            this.bt3.Name = "bt3";
            this.bt3.Size = new System.Drawing.Size(46, 42);
            this.bt3.TabIndex = 1;
            this.bt3.Text = "3";
            this.bt3.UseVisualStyleBackColor = true;
            this.bt3.Click += new System.EventHandler(this.bt3_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(101, 188);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(46, 42);
            this.button8.TabIndex = 1;
            this.button8.Text = "2";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // bt1
            // 
            this.bt1.Location = new System.Drawing.Point(34, 188);
            this.bt1.Name = "bt1";
            this.bt1.Size = new System.Drawing.Size(46, 42);
            this.bt1.TabIndex = 1;
            this.bt1.Text = "1";
            this.bt1.UseVisualStyleBackColor = true;
            this.bt1.Click += new System.EventHandler(this.bt1_Click);
            // 
            // bt6
            // 
            this.bt6.Location = new System.Drawing.Point(170, 124);
            this.bt6.Name = "bt6";
            this.bt6.Size = new System.Drawing.Size(46, 44);
            this.bt6.TabIndex = 1;
            this.bt6.Text = "6";
            this.bt6.UseVisualStyleBackColor = true;
            this.bt6.Click += new System.EventHandler(this.bt6_Click);
            // 
            // bt5
            // 
            this.bt5.Location = new System.Drawing.Point(101, 124);
            this.bt5.Name = "bt5";
            this.bt5.Size = new System.Drawing.Size(46, 44);
            this.bt5.TabIndex = 1;
            this.bt5.Text = "5";
            this.bt5.UseVisualStyleBackColor = true;
            this.bt5.Click += new System.EventHandler(this.bt5_Click);
            // 
            // bt4
            // 
            this.bt4.Location = new System.Drawing.Point(34, 124);
            this.bt4.Name = "bt4";
            this.bt4.Size = new System.Drawing.Size(46, 44);
            this.bt4.TabIndex = 1;
            this.bt4.Text = "4";
            this.bt4.UseVisualStyleBackColor = true;
            this.bt4.Click += new System.EventHandler(this.bt4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(170, 60);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(46, 45);
            this.button3.TabIndex = 1;
            this.button3.Text = "9";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // bt8
            // 
            this.bt8.Location = new System.Drawing.Point(101, 59);
            this.bt8.Name = "bt8";
            this.bt8.Size = new System.Drawing.Size(46, 46);
            this.bt8.TabIndex = 1;
            this.bt8.Text = "8";
            this.bt8.UseVisualStyleBackColor = true;
            this.bt8.Click += new System.EventHandler(this.bt8_Click);
            // 
            // bt7
            // 
            this.bt7.Location = new System.Drawing.Point(34, 59);
            this.bt7.Name = "bt7";
            this.bt7.Size = new System.Drawing.Size(46, 46);
            this.bt7.TabIndex = 1;
            this.bt7.Text = "7";
            this.bt7.UseVisualStyleBackColor = true;
            this.bt7.Click += new System.EventHandler(this.bt7_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("SimSun", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(109, 11);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 29);
            this.label5.TabIndex = 0;
            this.label5.Text = "Input";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.SkyBlue;
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.gbinput);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Location = new System.Drawing.Point(38, 13);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(979, 578);
            this.panel2.TabIndex = 7;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.MediumTurquoise;
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.tbDeposit);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(332, 67);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(312, 148);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(76, 97);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(139, 35);
            this.button4.TabIndex = 5;
            this.button4.Text = "Confirm";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("SimSun", 14F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(57, 29);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(207, 19);
            this.label9.TabIndex = 4;
            this.label9.Text = "Manual Transaction";
            // 
            // tbDeposit
            // 
            this.tbDeposit.Location = new System.Drawing.Point(147, 66);
            this.tbDeposit.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tbDeposit.Name = "tbDeposit";
            this.tbDeposit.Size = new System.Drawing.Size(144, 20);
            this.tbDeposit.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 66);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Amount Depositing:";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.MediumTurquoise;
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnDeposit5);
            this.groupBox1.Controls.Add(this.btnDeposit10);
            this.groupBox1.Controls.Add(this.btnDeposit50);
            this.groupBox1.Controls.Add(this.btnDeposit20);
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(665, 121);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(286, 301);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(71, 255);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(176, 13);
            this.label14.TabIndex = 9;
            this.label14.Text = "To deposit 50 euros on the account";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(71, 197);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(176, 13);
            this.label12.TabIndex = 7;
            this.label12.Text = "To deposit 20 euros on the account";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(71, 136);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(176, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "To deposit 10 euros on the account";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(71, 73);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(170, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "To deposit 5 euros on the account";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("SimSun", 14F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(57, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(196, 19);
            this.label3.TabIndex = 0;
            this.label3.Text = "Quick Transaction";
            // 
            // btnDeposit5
            // 
            this.btnDeposit5.Location = new System.Drawing.Point(6, 58);
            this.btnDeposit5.Name = "btnDeposit5";
            this.btnDeposit5.Size = new System.Drawing.Size(57, 43);
            this.btnDeposit5.TabIndex = 1;
            this.btnDeposit5.Text = "5";
            this.btnDeposit5.UseVisualStyleBackColor = true;
            this.btnDeposit5.Click += new System.EventHandler(this.btnDeposit5_Click);
            // 
            // btnDeposit10
            // 
            this.btnDeposit10.Location = new System.Drawing.Point(6, 121);
            this.btnDeposit10.Name = "btnDeposit10";
            this.btnDeposit10.Size = new System.Drawing.Size(57, 44);
            this.btnDeposit10.TabIndex = 1;
            this.btnDeposit10.Text = "10";
            this.btnDeposit10.UseVisualStyleBackColor = true;
            this.btnDeposit10.Click += new System.EventHandler(this.btnDeposit10_Click);
            // 
            // btnDeposit50
            // 
            this.btnDeposit50.Location = new System.Drawing.Point(6, 240);
            this.btnDeposit50.Name = "btnDeposit50";
            this.btnDeposit50.Size = new System.Drawing.Size(57, 44);
            this.btnDeposit50.TabIndex = 1;
            this.btnDeposit50.Text = "50";
            this.btnDeposit50.UseVisualStyleBackColor = true;
            this.btnDeposit50.Click += new System.EventHandler(this.btnDeposit50_Click);
            // 
            // btnDeposit20
            // 
            this.btnDeposit20.Location = new System.Drawing.Point(6, 182);
            this.btnDeposit20.Name = "btnDeposit20";
            this.btnDeposit20.Size = new System.Drawing.Size(57, 44);
            this.btnDeposit20.TabIndex = 1;
            this.btnDeposit20.Text = "20";
            this.btnDeposit20.UseVisualStyleBackColor = true;
            this.btnDeposit20.Click += new System.EventHandler(this.btnDeposit20_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("SimSun", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(430, 21);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(125, 29);
            this.label6.TabIndex = 0;
            this.label6.Text = "Deposit";
            // 
            // bt10
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1043, 593);
            this.Controls.Add(this.panel2);
            this.Name = "bt10";
            this.Text = "ATMLog";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.bt10_FormClosed);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.gbinput.ResumeLayout(false);
            this.gbinput.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbaccountidcode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel gbinput;
        private System.Windows.Forms.Button bt3;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button bt1;
        private System.Windows.Forms.Button bt6;
        private System.Windows.Forms.Button bt5;
        private System.Windows.Forms.Button bt4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button bt8;
        private System.Windows.Forms.Button bt7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnDeposit50;
        private System.Windows.Forms.Button btnDeposit20;
        private System.Windows.Forms.Button btnDeposit10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnDeposit5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbDeposit;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblRFIDstatus;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonNone;
        private System.Windows.Forms.Button buttonNone2;
    }
}

