﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Phidget22;
using Phidget22.Events;
using MySql.Data.MySqlClient;
using MySql.Data;
using Camping_Gate;

namespace InfoDesk
{
    class DBInfoDesk
    {
        public MySqlConnection connection;

        string name, account, rfid, ticket;
        int campingId, available, creditStored;
        CustomerCampInfo camper;
        public DBInfoDesk()
        {
            string connInfo = "server=studmysql01.fhict.local;" +
                                "database=dbi380048;" +
                                "user id=dbi380048;" +
                                "password=pcsb;";
            connection = new MySqlConnection(connInfo);
        }
        public List<CustomerInfo> GetAllPeople()
        {
            List<CustomerInfo> tempList = new List<CustomerInfo>();

            string sql = "SELECT * FROM accounts";
            MySqlCommand command = new MySqlCommand(sql, connection);

            try
            {
                connection.Open();
                MySqlDataReader reader = command.ExecuteReader();
                //Console.WriteLine("Got connection");

                while (reader.Read())
                {
                    //Console.WriteLine("Start reading");
                    name = Convert.ToString(reader["Name"]);
                    //Console.WriteLine("Got Name");
                    account = Convert.ToString(reader["Email"]);
                    //Console.WriteLine("Got Email");
                    rfid = Convert.ToString(reader["RFID"]);
                    //Console.WriteLine("Got RFID");
                    ticket = Convert.ToString(reader["TicketId"]);
                    //Console.WriteLine("Got Ticket");

                    creditStored = Convert.ToInt32(reader["CreditStored"]);

                    tempList.Add(new CustomerInfo(account, name, rfid, ticket, creditStored));
                    //Console.WriteLine("Got Object");
                }
            }
            catch
            {
                MessageBox.Show("error while loading the People into the list");
            }
            finally
            {
                connection.Close();
            }


             
            return tempList;
        }
        public CustomerCampInfo GetCamping(CustomerInfo c)
        {
            string sql2 = "Select * FROM tickets WHERE Id = '" + c.TicketNo + "'";
            MySqlCommand command2 = new MySqlCommand(sql2, connection);
            //Console.WriteLine("Set command 2");

            try
            {
                connection.Open();
                //Console.WriteLine("Connetion Open");

                MySqlDataReader reader = command2.ExecuteReader();
                //Console.WriteLine("Reader2 Open");

                while (reader.Read())
                {
                    campingId = Convert.ToInt32(reader["CampingId"]);
                   // Console.WriteLine("Got CampingId: " + campingId);

                    camper = new CustomerCampInfo(account, name, rfid, campingId);
                    //Console.WriteLine("Camper found");
                }
                reader.Close();
            }
            catch
            {
                Console.WriteLine("Getting camping crashed");
            }
            finally
            {
                connection.Close();
            }
            return camper;
        }

        private CustomerInfo SearchPerson(string id)
        {
            List<CustomerInfo> allPeople = GetAllPeople();

            foreach (CustomerInfo p in allPeople)
            {
                if (p.getId() == id)
                { return p; }
            }
            return null;
        }

        public void UpdateAccount(string id,string ticket)
        {
            String sql = "Update accounts SET TicketId = '" + ticket + "', PaidForTicket = '1' Where RFID = '" + id +"'";
            MySqlCommand command = new MySqlCommand(sql, connection);
            try
            {
                if (SearchPerson(id) == null)
                {
                    MessageBox.Show("Person doesn't exist");
                }
                else
                {
                    connection.Open();
                    command.ExecuteNonQuery();

                    //MessageBox.Show("Account Updated");
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Update account failed");
            }
            finally
            {
                connection.Close();
            }
        }
        public void UpdateCredit(string id, double credit)
        {
            String sql = "Update accounts SET CreditStored = '" + credit + "' Where RFID = '" + id + "'";
            MySqlCommand command = new MySqlCommand(sql, connection);
            try
            {
                if (SearchPerson(id) == null)
                {
                    MessageBox.Show("Person doesn't exist");
                }
                else
                {
                    connection.Open();
                    command.ExecuteNonQuery();

                    //MessageBox.Show("Credit Updated");
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Update Credit failed");
            }
            finally
            {
                connection.Close();
            }
        }
        public void UpdateTicket(int ticketId, int camping, int amountOfPeople, double Price)
        {
            String sql = "Update tickets SET CampingId = '" + camping + "', AmountOfPeople = '" + amountOfPeople + "', Price = '" + Price + "' Where Id = '" + ticketId + "'";
            MySqlCommand command = new MySqlCommand(sql, connection);
            try
            {
                    connection.Open();
                    command.ExecuteNonQuery();

                    //MessageBox.Show("Ticket Updated");
            }
            catch (Exception)
            {
                Console.WriteLine("Update ticket failed");
            }
            finally
            {
                connection.Close();
            }
        }


        public void MakeTicket(int ticketId, int camping, int amountOfPeople, double Price)
        {
            DateTime date = DateTime.Now;


            String sql = "INSERT INTO tickets VALUES (@ticket, @date, @camping, @amount, @Price)";
            MySqlCommand command = new MySqlCommand(sql, connection);
            Console.WriteLine("command passed");

            command.Parameters.AddWithValue("@ticket", ticketId);
            Console.WriteLine("ticketID: " + ticketId);
            command.Parameters.AddWithValue("@date", date);
            Console.WriteLine("Date: " + date);
            command.Parameters.AddWithValue("@camping", camping);
            Console.WriteLine("camping: " + camping);
            command.Parameters.AddWithValue("@amount", amountOfPeople);
            Console.WriteLine("Amount: " + amountOfPeople);
            command.Parameters.AddWithValue("@Price", Price);
            Console.WriteLine("Price: " + Price);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();

                //MessageBox.Show("Ticket Added to database");
            }
            catch (Exception)
            {
                Console.WriteLine("Creating ticket failed");
            }
            finally
            {
                connection.Close();
            }
        }

        public List<CustomerInfo> GetAllTickets()
        {
            List<CustomerInfo> tempList = new List<CustomerInfo>();

            string sql = "SELECT * FROM tickets";
            MySqlCommand command = new MySqlCommand(sql, connection);
            Console.WriteLine("Set Command ticket");

            try
            {
                connection.Open();
                Console.WriteLine("Connetion Open");

                MySqlDataReader reader = command.ExecuteReader();
                Console.WriteLine("Reader Open");

                while (reader.Read())
                {
                    ticket = Convert.ToString(reader["Id"]);
                    Console.WriteLine("Got Ticket: " + ticket);

                    tempList.Add(new CustomerInfo(ticket));

                    Console.WriteLine("Camping added");
                }

                reader.Close();
            }
            catch
            {
                Console.WriteLine("Getting Camps crashed");
            }
            finally
            {
                connection.Close();
            }

            return tempList;
        }

        public List<CampingInfo> GetAllCamping()
        {
            List<CampingInfo> tempList = new List<CampingInfo>();

            string sql = "SELECT * FROM campings";
            MySqlCommand command = new MySqlCommand(sql, connection);
            Console.WriteLine("Set Command camping");

            try
            {
                connection.Open();
                Console.WriteLine("Connetion Open");

                MySqlDataReader reader = command.ExecuteReader();
                Console.WriteLine("Reader Open");

                while (reader.Read())
                {
                    campingId = Convert.ToInt32(reader["Id"]);
                    Console.WriteLine("Got campingId: " + campingId);

                    available = Convert.ToInt32(reader["IsAvailable"]);
                    Console.WriteLine("Got available: " + available);

                    tempList.Add(new CampingInfo(campingId, available));

                    Console.WriteLine("Camping added");
                }

                reader.Close();
            }
            catch
            {
                Console.WriteLine("Getting Camps crashed");
            }
            finally
            {
                connection.Close();
            }

            return tempList;
        }
    }
}
