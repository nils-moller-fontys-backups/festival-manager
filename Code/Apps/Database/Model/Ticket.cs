﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Model
{
    public class Ticket
    {
        public int Id { get; set; }
        public DateTime DateOfPurchase { get; set; }
        public int? CampingId { get; set; }
        public Camping Camping { get; set; }
        public int AmountOfPeople { get; set; }
        public double Price { get; set; }
        public string OtherEmails { get; set; }
    }
}
