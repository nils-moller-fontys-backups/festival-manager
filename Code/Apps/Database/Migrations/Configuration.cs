namespace Database.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<RockfairContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RockfairContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            context.Consumables.AddOrUpdate(new Model.Consumable
            {
                Name = "Hamburger",
                Price = 6.00,
                Quantity = 200,
            });

            context.Consumables.AddOrUpdate(new Model.Consumable
            {
                Name = "Coca Cola",
                Price = 4.00,
                Quantity = 400,
            });

            context.Consumables.AddOrUpdate(new Model.Consumable
            {
                Name = "Small fries",
                Price = 3.50,
                Quantity = 200,
            });
        }
    }
}
