﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RFID_Prototype
{
    class Person
    {
        private string id;
        private string name;
        private int age;
        
        public string Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public int Age
        {
            get { return age; }
            set { age = value; }
        }
        public Person(string name, int age,string id)
        {
            this.Name = name;
            this.Age = age;
            this.Id = id;
        }

        public string getId()
        {
            return this.Id;
        }
        public void ChangeID(string id)
        {
            this.Id = id;
        }
        public override String ToString()
        {
            String holder;
            holder = "Id: " + this.Id + ", Name: " + this.Name + ", Age: " + this.age;
            return holder;
        }
    }
}
