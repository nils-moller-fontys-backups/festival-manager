﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Phidget22;
using Phidget22.Events;

namespace RFID_Prototype
{
    public partial class Form1 : Form
    {

        /*
        When registering a person to a tag,
        scan the card before clicking confirm  
        */


        RFID rfid;
        private People testSubjects = new People();
        public Form1()
        {
            InitializeComponent();
            //StartUp();
            this.addSomeTestingStuff();
        }

        public void StartUp()
        {
            try
            {
                rfid = new RFID();

                rfid.Attach += rfid_Attach;
                rfid.Detach += rfid_Detach;
                rfid.Error += rfid_Error;
                rfid.Tag += rfid_Tag;
                rfid.TagLost += rfid_TagLost;

                rfid.Open();
            }
            catch (PhidgetException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
            catch
            {
                MessageBox.Show("something else");
            }
        }

        private void rfid_Attach(object sender, AttachEventArgs e)
        {
            tbStatus.Text = "attached";
        }

        private void rfid_Detach(object sender, DetachEventArgs e)
        {
            tbStatus.Text = "detached";
        }

        private void rfid_Error(object sender, ErrorEventArgs e)
        {
            MessageBox.Show("error");
        }

        private void rfid_Tag(object sender, RFIDTagEventArgs e)
        {
            tbStatus.Text = e.Tag;
            tbIDset.Text = e.Tag;
            string reg = DateTime.Now.ToString() + " : " + e.Tag;
            lbTags.Items.Add(reg);

            try
            {
                lbTags.Items.Add(testSubjects.SearchPerson(e.Tag));
                tbNameRead.Text = testSubjects.SearchPerson(e.Tag).Name;
            }
            catch(Exception)
            {

            }
        }

        private void rfid_TagLost(object sender, RFIDTagLostEventArgs e)
        {
            tbStatus.Text = "ready";
            tbNameRead.Clear();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            rfid.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Person temp = new Person(tbNameSet.Text, Convert.ToInt32(tbAge.Text), tbIDset.Text);
                testSubjects.AddPerson(temp);
            }
            catch (Exception)
            {
                MessageBox.Show("Problem with registering new person");
            }
        }

        private void btn_Clear_Click(object sender, EventArgs e)
        {
            lbTags.Items.Clear();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tbStatus.Text = "ready";
            StartUp();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            rfid.Close();
            tbStatus.Text = "Stopped Reading";
        }


        private void addSomeTestingStuff()
        {
            this.testSubjects.AddPerson(new Person("Chris Hansame", 22, "2800b8532e"));
        }

        private void btnShowAll_Click(object sender, EventArgs e)
        {
            this.lbTags.Items.Clear();
            foreach (Person p in this.testSubjects.ShowAllPeople())
            {
                this.lbTags.Items.Add(p);
            }
        }
    }
}
